'use strict'

// create an endpoint to send an "EMP" signal
const express = require('express');
const app = express();
const moment = require('moment');

// import json data
var data = require("./nasa.json");
var http = require('http');

var time;

app.post('/emp', (req, res) => {
    res.send({
        msg: "EMP SENT"
    });
    console.log("EMP BLAST HAS BEEN DETECTED");
    console.log("SENDING FINAL BACKUP NOW");
    post();
    stop();
})

app.listen(3000, console.log("now listening for EMPs on port 3000"));

// define random data selector
function genData(){
    return data[Math.floor((Math.random() * 1000))];
}

// generate the name of the file
function genName(){
    return "backup_file" + Math.floor((Math.random() * 1000) + 1);
}

// post the data to the API endpoint
function post(){

    const time = moment().format('YYYY-DD-MM HH:mm:ss:SSS')
    // create the local data object to send
    const postData = JSON.stringify({
        fileName: genName(),
        data: genData(),
        transmitTime: time
    })

    // define options for the HTTP request
    const options = {
        hostname: '192.168.1.92',
        port: 8080,
        method: 'POST',
        path: '/api/upload',
        json: true,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length
        }
    }

    // create http request object
    const req = http.request(options, res => {
        console.log('File sent at: ' + time);
        
        req.on('error', error => {
            console.error(error)
        });
    });
      
    // Post data to the API
    req.write(postData)
    req.end()
}

// generate the data
function start() { 
    time = setInterval(() => post(), 3000); 
} 

function stop() { 
    clearInterval(time); 
} 

start();
